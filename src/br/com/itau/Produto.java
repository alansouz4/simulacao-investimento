package br.com.itau;

public class Produto {
    private double valor;
    private int meses;
    private double porcentagem = 0.7;
    private double rendimento;


    public void status(){
        System.out.println("Valor mensal a investido:  " + this.valor);
        System.out.println("Meses a investir: " + this.meses);
        System.out.println("Porcentagem mensal: " + this.porcentagem);
        System.out.println("Valor total com os rendimentos: " + this.rendimento);
    }


    public void calculoRendimento(){
        this.rendimento = ((this.valor * 12) * ((12 * 0.7)/100)) + (this.valor * 12);
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getMeses() {
        return meses;
    }

    public void setMeses(int meses) {
        this.meses = meses;
    }

    public double getPorcentagem() { return porcentagem; }

    public void setPorcentagem(double porcentagem) {
        this.porcentagem = porcentagem;
    }

    public double getRendimento() {
        return rendimento;
    }

    public void setRendimento(double rendimento) {
        this.rendimento = rendimento;
    }
}
