package br.com.itau;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Produto produto = new Produto();

        System.out.print("Qual o valor a ser investido ? ");
        produto.setValor(Double.parseDouble(sc.nextLine()));
        System.out.print("Deseja aplicar para quantos meses ? ");
        produto.setMeses(Integer.parseInt(sc.nextLine()));
        produto.calculoRendimento();
        produto.status();
    }
}
